# php-composer

Test project with:

* **Language:** PHP
* **Package Manager:** Composer

## How to use

Please see the [usage documentation](https://gitlab.com/gitlab-org/security-products/tests/common#how-to-use) for Security Products test projects.

## Supported Security Products Features

| Feature             | Supported          |
|---------------------|--------------------|
| SAST                | :white_check_mark: |
| Dependency Scanning | :white_check_mark: |
| Container Scanning  | :x:                |
| DAST                | :x:                |
| License Complience  | :white_check_mark: |
